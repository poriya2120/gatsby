
import React from "react"
import { Link } from "gatsby"
import { compose, graphql } from 'react-apollo'

import Layout from "../components/layout"
import SEO from "../components/seo"

const Blog = ({data}) => (
    <div>
    <SEO title="Home" />
    <h1>lastest posts</h1>
        {data.allMarkdownRemark.edges.map(post=>(
            <div key={post.node.id}>
                <h3>{post.node.frontmatter.title}</h3>
                <small>{post.node.frontmatter.auther} on 
                {post.node.frontmatter.date}
                </small>
                <br/>
                <br/>
                <Link to={post.node.frontmatter.path}>Read more</Link>
            </div>

        ))}
     </div>

)
export const pageQuery=graphql`
qurey BlogIndexQuery
{

    allMarkdownRemark {
      edges {
        node {
          forntmatter{
            path
            title
            data
            auter
          }
          
        }
      }
    }
  }
`

export default Blog