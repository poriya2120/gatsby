import React from 'react';
import Link from 'gatsby-link' ;
const Menu = () => (
     <div>
         <div style={{
             background:'#fff',
             paddingTop:'10px'
         }}>
             <ul style={{
                 listStyle:'none',
                 display:'flex',
                 justifyContent:'space-evenly'

             }}>
              
                     <li > <Link to="/">Home</Link></li>
                     <li > <Link to="/about">about</Link></li>
                     <li > <Link to="/service">service</Link></li>
                     
                 
             </ul>
         </div>

    </div>
    
    
    );

 
export default Menu;
