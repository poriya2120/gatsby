const path =require('path');
exports.createPages=({boundActionCreators,grapql})=>{
const {createPages}=boundActionCreators
const postTemplate=path.resolve('src/template/blog-post.js');
return grapql(`
allMarkdownRemark {
    edges {
      node {
        forntmatter{
          path
          title
          data
          auter
        }
        
      }
    }
  }


`).then(res=>{
    if(res.errors){
        return Promise.reject(res.errors)
    }
    res.data.allMarkdownRemark.edges.forEach(({node})=>{
        createPages({
            path: node.frontmater.path,
            components:postTemplate
        })

    })
}


)

}